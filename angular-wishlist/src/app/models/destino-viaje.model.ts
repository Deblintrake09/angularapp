export class DestinoViaje{
    nombre:string;
    imagenUrl:string;
    descripcion:string;

    constructor(nombre:string, url:string, desc:string)
    {
        this.nombre=nombre;
        this.imagenUrl=url;
        this.descripcion=desc;

    }
}